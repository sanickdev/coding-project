import React from 'react';
import './App.css';
import ColorMatchingGame from './containers/ColorMatchingGame/ColorMatchingGame';

function App() {
  return (
    <div className="App">
      <ColorMatchingGame />
    </div>
  );
}

export default App;
