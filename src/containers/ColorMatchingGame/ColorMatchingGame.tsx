import React, { Component } from 'react';
//import axios from 'axios';
import JSONFILE from './example.json';
import Row from './Table/Row/Row';
import classes from './ColorMatchingGame.module.css';

interface StateProps {
  id: string,
  m: number,
  n: number,
  positions: number[][],
  seconds: number,
  position: number[],
  result: number[][],
  dimenX: number, 
  stop: boolean
};

class ColorMatchingGame extends Component<{}, StateProps> {

  state = {
    id: JSONFILE.data.id,
    m: JSONFILE.data.m,
    n: JSONFILE.data.n ,
    positions: JSONFILE.data.state,
    seconds: 0,
    position: [],
    result: [],
    dimenX: 0,
    stop: true
  }

  componentWillMount() {
    let fillAll: any = this.state.result;
    for(let i = 0; i <= (this.state.m - 1) ; i++){
      fillAll.push([]);
    }
    this.setState({result: fillAll});
  }

  componentDidMount() {
    
    const interval = setInterval(() => {
      
      if(this.state.stop) return;
      if(this.state.m <= (this.state.dimenX + 1)) return;
      let updatedSecond = this.state.seconds;
      const positionX = this.getActualIndex(this.state.dimenX, this.state.seconds);
      const actualNumber = this.getActualNumber(positionX, this.state.seconds);

      let resultValue: any = this.state.result;
      resultValue[positionX].push(actualNumber);

      
      if(updatedSecond >= this.state.n){
        updatedSecond = 0;
        this.setState({dimenX: positionX});
      }
      
      updatedSecond++;
      this.setState({seconds: updatedSecond});
      this.setState({result: resultValue});
  }, 1000);
  return () => clearInterval(interval);
    
    /*
    axios.get('http://coding-project.imtlab.io/seed').then(response => {
      console.log(response);
      this.setState( {} );
      //this.setState{colors: response.data};
    }).catch(error => {
      console.error(error);
    });
    */
  }

  getActualIndex = (actual: number, second: number) => {
    let number = actual;
    if(second === this.state.n){
      number = number + 1;
    }
    return number;
  }

  getActualNumber = (i: number, y: number) => {
    const number = this.state.positions[i][y];
    return number;
  }

  stopInterval = () => {
    let stop = this.state.stop;
    this.setState({stop: !stop});
  }
  

  render() {
    
    let render = <p>Loading...</p>;
    if(this.state !== null){
      render = (
        <div className={classes.ColorMatchingGame}>
          <div>
            <button className={classes.Button} onClick={this.stopInterval}>{this.state.stop ? 'Start' : 'Stop'}</button>
          </div>
          <br />
          <table>
            <tbody>
            {this.state.result.map((row: any, index: number) => {
              return <Row key={index} data={row} position={this.state.seconds} limit={this.state.n} />;
            })}
            </tbody>
          </table>
        </div>
      );
    }
    return render;
  }
}


export default React.memo(ColorMatchingGame);
