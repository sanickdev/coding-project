import React, {Component} from 'react';
import classes from './Cell.module.css';

interface StateProps {
    data: number,
    history: number[],
    position: number,
    limit: number
  };
  

class Cell extends Component<StateProps> {

    getStateCell = (position: number, history: number[], data: number, limit: number) => {
        let stateCell = 'DEAD';
        if(this.props.history.length >= 3){
            let historyPosition = this.props.position - 1;
            const firstLeft = this.props.history[historyPosition - 1];
            const secondLeft = this.props.history[historyPosition - 2];
            //-------------
            let firstRight = null;
            let secondRight = null;
            let thirdRight = null;
            if(position <= (limit - 1)){
                firstRight = this.props.history[historyPosition + 1];
            } else if(position <= (limit - 2)) {
                firstRight = this.props.history[historyPosition + 1];
                secondRight = this.props.history[historyPosition + 2];
            } else if(position <= (limit - 3)) {
                firstRight = this.props.history[historyPosition + 1];
                secondRight = this.props.history[historyPosition + 2];
                thirdRight = this.props.history[historyPosition + 3];
            }

            if(this.props.history.length >= 4){
                const thirdLeft = this.props.history[historyPosition - 3];
                if((firstRight === 0 && secondRight === 0 && thirdRight === 0) 
                    && (firstLeft === 0 && secondLeft === 0 && thirdLeft === 0)
                    && (data === 0)){
                    //Any dead cell with exactly three sad neighbors becomes a sad cell.
                    stateCell = 'SAD';
                }
                if( (firstLeft === 1 || secondLeft === 1 || thirdLeft === 1)  
                    && (firstRight === 1 || secondRight === 1 || thirdRight === 1) 
                    && ((firstLeft === 2 && secondLeft === 2  && thirdLeft === 2 ) 
                    || (firstRight === 2 && secondRight === 2 && thirdRight === 2))
                    
                    && (data === 0)){
                      //Any dead cell with exactly two sad neighbors and 1 happy neighbor becomes a sad cell.
                      stateCell = 'SAD';
                }
                if((firstLeft === 0 || secondLeft === 0 || thirdLeft === 0 || firstRight === 0 
                        || secondRight === 0 || thirdRight === 0) 
                    && (firstLeft === 2 || secondLeft === 2 || thirdLeft === 2 || firstRight === 2 
                        || secondRight === 2 || thirdRight === 2)
                    && (data === 0)){
                    //Any dead cell with exactly one sad neighbor and 2 happy neighbors becomes a happy cell.
                    stateCell = 'HAPPY';
                }
                if((firstLeft === 2 && secondLeft === 2 && thirdLeft === 0 && firstRight === 2  && secondRight === 2 && thirdRight === 2)
                    && (data === 0)){
                    //Any dead cell with exactly 3 happy neighbors becomes a happy cell.
                    stateCell = 'HAPPY';
                }
                if( (firstLeft === 1 && secondLeft === 1 && thirdLeft === 1) 
                    && (firstRight === 2 && firstRight === 2 && thirdRight === 2)
                    && (data === 1 || data === 2)){
                    //Any sad or happy cell with two or three sad or happy neighbours survives
                    stateCell = '';
                }
            }
            
        }
        return stateCell;
    }


    render() {
        let render = null;
        let data = this.props.data;
        let stateCell = this.getStateCell(this.props.position, this.props.history, data, this.props.limit);

        switch(stateCell) {
            case 'DEAD':
                render= <td className={classes.Dead}>{data}</td>;
                break;
            case 'SAD':
                render= <td className={classes.Sad}>{data}</td>;
                break;
            case 'HAPPY':
                render= <td className={classes.Happy}>{data}</td>;
                break;
            default:
                render = <td>{data}</td>;
                break;
        }
        return render;
    }
}

export default Cell;