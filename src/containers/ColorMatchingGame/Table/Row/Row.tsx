import React, {Component} from 'react';
import Cell from './Cell/Cell';

interface StateProps {
  data: number[],
  position: number,
  limit: number
};


class Row extends Component<StateProps> {
  render() {
    return (
      <tr>
        {this.props.data.map((cell: any, index: number) => {
          return <Cell history={this.props.data} key={index} data={cell} position={this.props.position} limit={this.props.limit}/>
        })}
      </tr>
    );
  }
}

export default Row;